package com.jobinlawrance.koindemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    val mainViewModel by viewModel<MainViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val textView = findViewById<TextView>(R.id.text_view)
        mainViewModel.viewModelScope.launch {
            try {
                val title = mainViewModel.getSomeVal().title
                textView.text = title
                Log.d("###", "Title is $title")
            } catch (exception: Exception) {
                textView.text = exception.localizedMessage
                Log.e("###","Error", exception)
            }
        }
    }
}