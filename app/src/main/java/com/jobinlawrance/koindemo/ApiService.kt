package com.jobinlawrance.koindemo

import com.google.gson.annotations.SerializedName
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET

interface ApiService {
    @GET("https://jsonplaceholder.typicode.com/todos/1")
    suspend fun getSomeVal(): SomeVal
}

data class SomeVal(@SerializedName("title") val title: String)
