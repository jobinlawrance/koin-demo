package com.jobinlawrance.koindemo

import android.app.Application
import com.jobinlawrance.koindemo.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.koinApplication


class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        // Creating the dependency graph
        startKoin {
            androidContext(this@MainApplication)
            modules(viewModelModule)
        }
    }
}