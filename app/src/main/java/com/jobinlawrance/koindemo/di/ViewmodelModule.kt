package com.jobinlawrance.koindemo.di

import com.jobinlawrance.koindemo.ApiService
import com.jobinlawrance.koindemo.MainViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


val viewModelModule = module {

    // Singleton
    single<ApiService> {
        Retrofit.Builder().baseUrl("https://google.com")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiService::class.java)
    }

    single {"Daatabaser"}

    viewModel { MainViewModel(get(), get(), androidContext()) }
}