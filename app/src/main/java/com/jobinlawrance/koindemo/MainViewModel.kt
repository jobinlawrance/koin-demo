package com.jobinlawrance.koindemo

import android.content.Context
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.map

class MainViewModel(private val apiService: ApiService,val db: String ,val context: Context): ViewModel() {
    suspend fun getSomeVal() = apiService.getSomeVal()
}